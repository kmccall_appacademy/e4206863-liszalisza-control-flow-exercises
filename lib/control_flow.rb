# EASY

# Return the argument with all its lowercase characters removed.
def destructive_uppercase(str)
  str.each_char.select do |ch|
    ch != ch.downcase
  end.join
end

# Return the middle character of a string. Return the middle two characters if
# the word is of even length, e.g. middle_substring("middle") => "dd",
# middle_substring("mid") => "i"
def middle_substring(str)
  mid_index = str.length/2
  if str.length.odd?
    str[mid_index]
  else
    str[mid_index - 1, 2]
  end
end

# Return the number of vowels in a string.
def num_vowels(str)
  str.downcase.count("aeiou")
end

# Return the factoral of the argument (num). A number's factorial is the product
# of all whole numbers between 1 and the number itself. Assume the argument will
# be > 0.
def factorial(num)
  (1..num).reduce(:*)
end


# MEDIUM

# Write your own version of the join method. separator = "" ensures that the
# default seperator is an empty string.
def my_join(arr, separator = "")
  return "" if arr.empty?
  arr.inject do |sum, element|
    sum += separator + element
  end
end

# Write a method that converts its argument to weirdcase, where every odd
# character is lowercase and every even is uppercase, e.g.
# weirdcase("weirdcase") => "wEiRdCaSe"
def weirdcase(str)
  str.chars.map.with_index do |ch, i|
   if i.even?
      ch.downcase
    else
      ch.upcase
    end
  end.join
end

# Reverse all words of five more more letters in a string. Return the resulting
# string, e.g., reverse_five("Looks like my luck has reversed") => "skooL like
# my luck has desrever")
def reverse_five(str)
  str.split.map do |word|
    if word.length >= 5
      word.reverse
    else
      word
    end
  end.join(" ")
end

# Return an array of integers from 1 to n (inclusive), except for each multiple
# of 3 replace the integer with "fizz", for each multiple of 5 replace the
# integer with "buzz", and for each multiple of both 3 and 5, replace the
# integer with "fizzbuzz".
def fizzbuzz(n)
  (1..n).map do |i|
    i % 15 == 0 ? "fizzbuzz" : i % 5 == 0 ? "buzz" : i % 3 == 0 ? "fizz" : i
  end
end


# HARD

# Write a method that returns a new array containing all the elements of the
# original array in reverse order.
def my_reverse(arr)
  reversed = []
  (arr.length - 1).downto(0) do |i|
    reversed << arr[i]
  end
  reversed
end

# Write a method that returns a boolean indicating whether the argument is
# prime.
def prime?(num)
  return false if num < 2
  (2...num).each do |i|
    return false if num % i == 0
  end
  true
end

# Write a method that returns a sorted array of the factors of its argument.
def factors(num)
  (1..num).select do |i|
    num % i == 0
  end
end

# Write a method that returns a sorted array of the prime factors of its argument.
def prime_factors(num)
  (1..num).select do |i|
    num % i == 0 && prime?(i)
  end
end

# Write a method that returns the number of prime factors of its argument.
def num_prime_factors(num)
  prime_factors(num).length
end


# EXPERT

# Return the one integer in an array that is even or odd while the rest are of
# opposite parity, e.g. oddball([1,2,3]) => 2, oddball([2,4,5,6] => 5)
def oddball(arr)
  evens = []
  odds = []
  arr.each do |num|
    evens << num if num.even?
    odds << num if num.odd?
  end
  evens.length < odds.length ? evens[0] : odds[0]
end
